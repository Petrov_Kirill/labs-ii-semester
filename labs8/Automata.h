#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

enum STATES {Off, Waint, Accept, Cook};

#define MaxLeng 256

typedef struct 
{
		char drink [MaxLeng];
		unsigned char price;
} MENU;

class Automata
{
public:
	Automata ();
	void on();
	void off();
	
   void printMenu();
   void printState();

   int restMoney () { return cash; };

private:
	STATES State;
	int cash;
	int SelectedDrink;
	MENU *arr;
	int sizedrink;

	void coin();
	void choice();
	void check();
	void cancel();
	void cook();
	void finish();

};