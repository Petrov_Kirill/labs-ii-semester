#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "readling.h"



int NumberBooks (FILE *fp)
{
	int size=0;
	char buf [MaxStr];
	while ( ( fgets(buf,MaxStr,fp) )!=NULL )
		size++;
	return size/3;
}

void FillingStruct ( BOOK *p, FILE *fp)
{
	char buf [MaxStr];
	fgets(p->title, MaxStr, fp);
	fgets(p->fio, MaxStr, fp);
	fgets(buf, MaxStr, fp);
	p->year=atoi(buf);
}

BOOK *readling ( int *N )
{
	FILE *fp;
	BOOK *arr;
	int i; 

	if ( ( fp=fopen ( "in.txt", "rt") ) == NULL )
	{
		puts ("file does not exist");
		exit (1);
	}

	*N=NumberBooks(fp);

	rewind(fp);

	arr = (BOOK *) malloc ( (*N) *sizeof (BOOK) );

	if ( arr == NULL )
	{
		puts ("memory allocation error");
		exit (2);
	}

	for (i=0; i<*N; i++)
		FillingStruct ( &arr[i], fp );
	return arr;
}


