#include <stdio.h>
#include "readling.h"
#include <stdlib.h>
#include <locale.h>

int main ()
{
	BOOK *p=NULL;
	int N=0, i;
	setlocale (LC_ALL, "rus");
	p=readling (&N);
	for (i=0; i<N; i++)
		printf ("%s%s%d\n", p[i].title, p[i].fio, p[i].year);
	free (p);
	return 0;
}
