#include "..//circle.h"
#include <cstdio>

int main ()
{
	Circle Cr;
	double CostConcrete = 1000.0;
	double CostFence = 2000.0;
	const double RadiusPool = 3.0;
	const double TrackWidth = 1.0;
	double TrackArea,BasinArea,TrackLength;
	Cr.SetRad (RadiusPool);
	BasinArea=Cr.area();
	Cr.SetRad (RadiusPool+TrackWidth);
	TrackArea=Cr.area()-BasinArea;
	TrackLength=Cr.ference();
	CostConcrete*=TrackArea;
	CostFence*=TrackLength;
	printf ("%lf rub\n%lf rub\n", CostConcrete, CostFence);
	return 0;
}