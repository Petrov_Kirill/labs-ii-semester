#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../EnterLines.h"

int main()
{
	int N=0, i;
	char **ptr;

	ptr=EnterLines ( &N );

	for (i=N; i>=0; i--)
		printf ("%s", ptr[i]);

	return 0;
}