#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../EnterLines.h"
#include <time.h>

// функция составляющий случайную последовательность
void Permutation (int * arr, int *i, int min, int max)
{
	int N;
	if ( (max-min) != 0 ) 
	{
		N=min+rand ()% (max-min); //[min..max-1]
		arr[(*i)++] = N;
		Permutation (arr, i, min, N);
		Permutation (arr, i, N+1, max);
	}
}

int main()
{
	int N=0, i, j;
	char flag;
	char **ptr;
	int *p;
	srand (time (0));
	ptr=EnterLines (&N);
	p=(int *)malloc(N*sizeof(int));

	Permutation (p, &i, 0, N);

	for (i=0; i<N; i++)
		printf("%s", ptr[p[i]]);

	free (p);
	return 0;
}