#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <math.h>

#define MAX 256

typedef struct
{
	int SizeChar;
	int SizeWord;
	int SizeSign;
	int SizeNumbers;
	int MidleWord;
	unsigned char PopulChar;
} STATIK;

int CountWordStr ( const char *str )
{
	char inWord = 1;
	int count = 0;
	while (*str)
	{
		if ((*str !=' ') && inWord && *str )
		{
			inWord = 0;
			count++;
		}
		if (*str ==' ')
			inWord = 1;
		str++;
	}
	return count;
}

int CountSignWord (const char *str )
{
	int count=0;
	while ( *str  )
	{
		if (( *str=='.')||(*str==',')||(*str=='!')||(*str=='?'))
			count++;
		str++;
	}
	return count;
}
 
int CountNumbersWord (const char *str)
{
	int count=0;
	while (*str)
	{
		if ( (*str>='0')&&(*str<='9') )
			count++;
		str++;
	}
	return count;
}

int LengWords (char *str)
{ 
	char inWord = 1;
	int count = 0;
	char *StartWord;
	while (*str)
	{
		if ((*str !=' ') && inWord && *str )
		{
			inWord = 0;
			StartWord=str;
			while (*StartWord && *StartWord!=' ' )
			{
				count++;
				StartWord++;
			}
		}
		else
			if (*str == ' ')
				inWord = 1;	
		str++;
	}
	return count;
}

void PrintfStatic ( STATIK * Info )
{
	printf ("the number of symbols - %d\n", Info -> SizeChar);
	printf ("the number of words - %d\n", Info -> SizeWord);
	printf ("number of punctuation marks - %d\n", Info -> SizeSign);
	printf ("number of digits - %d\n", Info -> SizeNumbers);
	printf ("the average length of a word - %d\n", Info -> MidleWord);
	printf ("the most popular character - '%c'\n", Info -> PopulChar);
}

int main (int argc, char* argv[])
{
	STATIK Info;
	FILE *fp;
	int count=0, i=0, max=0, leng;
	char buf [MAX];
	char **data=NULL;
	int tmp [MAX]={0};
	setlocale(LC_ALL, "rus");
	if (argc < 2)
    {
		puts ("Invalid parameter transfer in main");
		exit (-1);
	}
	printf ("%d\n%s\n", argc, argv [1]);
	fp=fopen (argv[1], "rt");
	if (fp==NULL)
	{
		puts ("File not found");
		exit (1);
	}
	while ( fgets (buf, MAX, fp) )
		count++;
	rewind(fp);
	data = ( char ** ) malloc ( count*sizeof(char*) );
	while ( fgets(buf, MAX, fp) )
	{
		leng = strlen (buf);
		if (leng>=MAX ) exit (2);
		buf[leng-1]=0;
		data [i] = (char*) malloc ( (strlen(buf)+1) *sizeof (char) );
		strcpy ( data[i], buf );
		i++;
	}

	Info.SizeChar=0;
	for (i=0; i<count; i++)
		Info.SizeChar=Info.SizeChar+strlen ( data [i] );
	

	Info.SizeWord=0;
	for (i=0; i<count; i++)
		Info.SizeWord+=CountWordStr ( data[i] );
	
		
	Info.SizeSign=0;
	for (i=0; i<count; i++)
		Info.SizeSign+=CountSignWord ( data[i] );
	

	Info.SizeNumbers=0;
	for (i=0; i<count; i++)
		Info.SizeNumbers+=CountNumbersWord ( data[i] );
	 

	Info.MidleWord=0;
	for (i=0; i<count; i++)
		Info.MidleWord+=LengWords ( data[i] );

	Info.MidleWord=floorf( (float) Info.MidleWord/Info.SizeWord + 0.5f);
	


	for (i=0; i<count; i++)
		while ( *data[i] )
			tmp[ (unsigned char) *data[i]++ ]++;

	for (i=0; i<MAX; i++)
		if ( tmp[i]>max )
		{
			max=tmp[i];
			Info.PopulChar=i;
		}

	PrintfStatic ( &Info );

	for (i=0; i<count; i++)
		free ( data[i] );
	free (data);

}
